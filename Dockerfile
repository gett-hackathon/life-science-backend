FROM golang:1.11 AS builder
WORKDIR /go/src/gitlab.com/gett-hackathon/life-science-backend
COPY . .
RUN GO111MODULE=on CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o server .

FROM scratch
COPY --from=builder /go/src/gitlab.com/gett-hackathon/life-science-backend/server .
COPY --from=builder /go/src/gitlab.com/gett-hackathon/life-science-backend/static ./static
ENTRYPOINT ["./server"]
