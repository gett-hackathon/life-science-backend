package main

import (
	"flag"
	"fmt"

	"github.com/labstack/echo/middleware"
	"gitlab.com/gett-hackathon/life-science-backend/patient"

	"gopkg.in/mgo.v2"

	"github.com/labstack/echo"
)

func main() {
	pwd := flag.String("dbpw", "wefwefWEF43rsdef", "")
	host := flag.String("dbhost", "mongodb:27017", "")
	usr := flag.String("dbusr", "admin", "")
	dbn := flag.String("db", "meco", "")
	flag.Parse()

	// Create a session which maintains a pool of socket connections
	// to our MongoDB.
	session, err := mgo.Dial(fmt.Sprintf("mongodb://%v:%v@%v", *usr, *pwd, *host))
	if err != nil {
		panic(err)
	}

	db := session.DB(*dbn)

	server := echo.New()
	apigroup := server.Group("/api/v1")
	patient.RegisterHandler(apigroup, patient.NewService(patient.NewMongoStore(db)))
	server.Use(middleware.Logger())
	server.Static("/", "static")

	if err := server.Start(":8912"); err != nil {
		panic(err)
	}
}
