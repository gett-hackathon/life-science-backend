package patient

import (
	"errors"
	"time"

	"gitlab.com/gett-hackathon/life-science-backend/report"
	tokengen "gitlab.com/gett-hackathon/life-science-backend/tokengen"
)

type Service struct {
	store Store
}

func NewService(store Store) *Service {
	return &Service{
		store: store,
	}
}

func (service *Service) GetPatient(id string) (*Patient, error) {
	return service.store.Get(id)
}

func (service *Service) UpdatePatient(id string, patient *Patient) error {
	patient.ID = id
	return service.store.Update(patient)
}

func (service *Service) RegisterPatient(patient *Patient) error {
	return service.store.Register(patient)
}

func (service *Service) GetPatientWithAccessToken(id, accessToken string) (*Patient, error) {
	patient, err := service.store.Get(id)
	if err != nil {
		return nil, err
	}

	if err = service.VerifyAccesscode(id, accessToken); err != nil {
		return nil, err
	}

	return patient, nil
}

func (service *Service) AddMedicalReportWithAccessToken(id, accessToken string, report *report.Medical) error {
	patient, err := service.store.Get(id)
	if err != nil {
		return err
	}

	if err = service.VerifyAccesscode(id, accessToken); err != nil {
		return err
	}

	patient.MedicalReports = append(patient.MedicalReports, report)
	return service.store.Update(patient)
}

func (service *Service) VerifyAccesscode(id, accessToken string) error {
	patient, err := service.store.Get(id)

	tokenID, err := tokengen.HashToken(accessToken)
	if err != nil {
		return err
	}

	expires, ok := patient.AccessCodes[tokenID]
	if !ok {
		return errors.New("Invalid access code")
	}

	if expires.After(time.Now()) {
		return errors.New("Code expired")
	}

	return nil
}

func (service *Service) CreateDiseaseReport(id string, report *report.Disease) (string, error) {

	patient, err := service.store.Get(id)
	if err != nil {
		return "", err
	}

	patient.DiseaseReport = report
	if err = service.store.Update(patient); err != nil {
		return "", err
	}

	return service.GenerateAccessCode(id)
}

func (service *Service) AddDiseaseReport(id string, report *report.Disease) error {
	patient, err := service.store.Get(id)
	if err != nil {
		return err
	}

	if patient.MedicalReports == nil {
		return errors.New("Patient has no active report")
	}

	patient.DiseaseReport.SubReports = append(patient.DiseaseReport.SubReports, report)

	return service.store.Update(patient)
}

func (service *Service) DeleteDiseaseReport(id string) error {
	patient, err := service.store.Get(id)
	if err != nil {
		return err
	}

	if patient.MedicalReports == nil {
		return errors.New("Patient has no active report")
	}

	//TODO: Medical Report
	patient.DiseaseReport = nil

	return service.store.Update(patient)
}

func (service *Service) AddMedicalReport(id string, record *report.Medical) error {
	patient, err := service.store.Get(id)
	if err != nil {
		return err
	}

	patient.MedicalReports = append(patient.MedicalReports, record)

	return service.store.Update(patient)
}

func (service *Service) GenerateAccessCode(id string) (string, error) {
	patient, err := service.store.Get(id)
	if err != nil {
		return "", err
	}

	token, err := tokengen.GenerateToken(10)
	if err != nil {
		return "", nil
	}

	tokenID, err := tokengen.HashToken(token)
	if err != nil {
		return "", nil
	}

	patient.AccessCodes[tokenID] = time.Now().Add(time.Hour * 24)

	return tokenID, service.store.Update(patient)
}

func (service *Service) GetPublicByCoordinates(minLat, minLang, maxLat, maxLang float64) ([]*Patient, error) {
	return service.store.GetPublicByCoordinates(minLat, minLang, maxLat, maxLang)
}
