package patient

import (
	"net/http"
	"time"

	"github.com/labstack/echo"
	"gitlab.com/gett-hackathon/life-science-backend/report"
	tokengen "gitlab.com/gett-hackathon/life-science-backend/tokengen"
)

type Handler struct {
	group   *echo.Group
	service *Service
}

func RegisterHandler(group *echo.Group, service *Service) {
	handler := &Handler{
		group:   group,
		service: service,
	}

	group.POST("/patient/register", handler.RegisterPatient)

	group.GET("/patient/map", handler.GetPublicPatientInfoByCoordinates)

	group.GET("/patient", handler.GetPatient, handler.AuthenticationMiddleware)
	group.PATCH("/patient", handler.UpdatePatient, handler.AuthenticationMiddleware)
	group.GET("/patient/accesscode", handler.GenerateAccessCode, handler.AuthenticationMiddleware)
	group.POST("/patient/diseasereport", handler.CreateDiseaseReport, handler.AuthenticationMiddleware)
	group.PUT("/patient/diseasereport", handler.AddDiseaseReport, handler.AuthenticationMiddleware)
	group.DELETE("/patient/diseasereport", handler.DeleteDiseaseReport, handler.AuthenticationMiddleware)
}

func (handler *Handler) RegisterPatient(ctx echo.Context) error {
	payload := &RegisterPatientPayload{}
	err := ctx.Bind(payload)
	if err != nil {
		return err
	}

	token, err := tokengen.GenerateToken(10)
	if err != nil {
		return err
	}

	id, err := tokengen.HashToken(token)
	if err != nil {
		return err
	}

	err = handler.service.RegisterPatient(&Patient{
		AccessCodes:      make(map[string]time.Time),
		BirthDay:         payload.BirthDay,
		DiseaseReport:    nil,
		Gender:           payload.Gender,
		ID:               id,
		IsPregnant:       payload.IsPregnant,
		Location:         payload.Location,
		LastLocationDate: payload.LastLocationDate,
		MedicalReports:   []*report.Medical{},
	})
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, map[string]string{"patientToken": token})
}

func (handler *Handler) GetPublicPatientInfoByCoordinates(ctx echo.Context) error {
	payload := &GetPublicPatientInfoByCoordinatesPayload{}
	if err := ctx.Bind(payload); err != nil {
		return err
	}

	infos, err := handler.service.GetPublicByCoordinates(payload.MinLat, payload.MinLang, payload.MaxLat, payload.MaxLang)
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, infos)
}

func (handler *Handler) GetPatient(ctx echo.Context) error {
	patient := ctx.Get("patient").(*Patient)

	return ctx.JSON(http.StatusOK, patient)
}

func (handler *Handler) UpdatePatient(ctx echo.Context) error {
	patient := ctx.Get("patient").(*Patient)

	payload := &Patient{}
	err := ctx.Bind(payload)
	if err != nil {
		return err
	}

	return handler.service.UpdatePatient(patient.ID, payload)
}

func (handler *Handler) GenerateAccessCode(ctx echo.Context) error {
	patient := ctx.Get("patient").(*Patient)

	code, err := handler.service.GenerateAccessCode(patient.ID)
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, map[string]string{"accesscode": code})
}

func (handler *Handler) AddMedicalReport(ctx echo.Context) error {
	patient := ctx.Get("patient").(*Patient)

	report := &report.Medical{}
	err := ctx.Bind(report)
	if err != nil {
		return err
	}

	return handler.service.AddMedicalReport(patient.ID, report)
}

func (handler *Handler) DeleteDiseaseReport(ctx echo.Context) error {
	patient := ctx.Get("patient").(*Patient)
	return handler.service.DeleteDiseaseReport(patient.ID)
}

func (handler *Handler) AddDiseaseReport(ctx echo.Context) error {
	patient := ctx.Get("patient").(*Patient)

	report := &report.Disease{}
	err := ctx.Bind(report)
	if err != nil {
		return err
	}

	return handler.service.AddDiseaseReport(patient.ID, report)
}

func (handler *Handler) CreateDiseaseReport(ctx echo.Context) error {
	patient := ctx.Get("patient").(*Patient)

	report := &report.Disease{}
	err := ctx.Bind(report)
	if err != nil {
		return err
	}

	code, err := handler.service.CreateDiseaseReport(patient.ID, report)
	if err != nil {
		return err
	}

	return ctx.JSON(http.StatusOK, map[string]string{"accessCode": code})
}

func (handler *Handler) AddMedicalReportWithAccessToken(ctx echo.Context) error {
	payload := &AddMedicalReportPayload{}
	err := ctx.Bind(payload)
	if err != nil {
		return err
	}

	return handler.service.AddMedicalReportWithAccessToken(payload.PatientID, payload.AccessToken, payload.Report)
}
