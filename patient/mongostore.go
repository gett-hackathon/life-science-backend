package patient

import (
	tokengen "gitlab.com/gett-hackathon/life-science-backend/tokengen"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

const (
	PatientCollectionName = "patients"
)

type mongoStore struct {
	collection *mgo.Collection
}

func NewMongoStore(db *mgo.Database) Store {
	return &mongoStore{
		db.C(PatientCollectionName),
	}
}

func (store *mongoStore) Register(patient *Patient) error {
	return store.collection.Insert(patient)
}

func (store *mongoStore) Get(id string) (*Patient, error) {
	patient := &Patient{}
	err := store.collection.FindId(id).One(patient)
	return patient, err
}

func (store *mongoStore) GetByToken(token string) (*Patient, error) {
	id, err := tokengen.HashToken(token)
	if err != nil {
		return nil, err
	}

	patient := &Patient{}
	err = store.collection.FindId(id).One(patient)
	return patient, err
}

func (store *mongoStore) Update(patient *Patient) error {
	return store.collection.UpdateId(patient.ID, patient)
}

func (store *mongoStore) Delete(id string) error {
	return store.collection.RemoveId(id)
}

func (store *mongoStore) GetPublicByCoordinates(minLat, minLang, maxLat, maxLang float64) ([]*Patient, error) {
	patients := []*Patient{}

	err := store.collection.Find(bson.M{
		"location.lat":  bson.M{"$gte": minLat, "$lte": maxLat},
		"location.lang": bson.M{"$gte": minLang, "$lte": maxLang},
		"diseaseReport": bson.M{"$ne": nil},
	}).Select(bson.M{
		"location.lat":  1,
		"location.lang": 1,
		"isPregnant":    1,
		"birthDay":      1,
		"diseaseReport": 1,
	}).Limit(1000).All(&patients)

	return patients, err
}
