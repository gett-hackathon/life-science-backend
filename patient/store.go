package patient

type Store interface {
	Register(patient *Patient) error
	Get(id string) (*Patient, error)
	GetByToken(token string) (*Patient, error)
	Update(patient *Patient) error
	Delete(id string) error
	GetPublicByCoordinates(lat, long, latRange, langRange float64) ([]*Patient, error)
}
