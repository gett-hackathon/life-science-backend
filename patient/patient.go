package patient

import (
	"time"

	"gitlab.com/gett-hackathon/life-science-backend/report"
)

type Patient struct {
	ID               string               `json:"id" bson:"_id"`
	Gender           string               `json:"gender"`
	IsPregnant       bool                 `json:"isPregnant"`
	BirthDay         time.Time            `json:"birthday"`
	Location         *Location            `json:"location"`
	LastLocationDate time.Time            `json:"lastLocationDate"`
	MedicalReports   []*report.Medical    `json:"medicalReport"`
	DiseaseReport    *report.Disease      `json:"diseaseReport,omitempty" bson:"diseaseReport"`
	AccessCodes      map[string]time.Time `json:"accessCodes"`
}

type Location struct {
	Lat  float64 `json:"lat"`
	Lang float64 `json:"lang"`
}
