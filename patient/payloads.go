package patient

import (
	"time"

	"gitlab.com/gett-hackathon/life-science-backend/report"
)

type IDPayload struct {
	ID string
}

type AddMedicalReportPayload struct {
	AccessToken string
	PatientID   string
	DoctorID    string
	Report      *report.Medical
}

type RegisterPatientPayload struct {
	Gender           string    `json:"gender"           binding:"required"`
	BirthDay         time.Time `json:"birthday"         binding:"required"`
	IsPregnant       bool      `json:"isPregnant"       binding:"required"`
	Location         *Location `json:"location"         binding:"required"`
	LastLocationDate time.Time `json:"lastLocationDate" binding:"required"`
}

type GetPublicPatientInfoByCoordinatesPayload struct {
	MaxLat  float64 `query:"maxLat"  binding:"required"`
	MaxLang float64 `query:"maxLang" binding:"required"`
	MinLat  float64 `query:"minLat"  binding:"required"`
	MinLang float64 `query:"minLang" binding:"required"`
}
